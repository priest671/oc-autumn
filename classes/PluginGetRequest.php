<?php namespace Jd\Autumn\Classes;

use Illuminate\Http\Request;

/**
 * Process request for PluginGet
 */
class PluginGetRequest
{
    private $request = null;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function getName() {
        return $this->request->input("name");
    }

    public function getServer() {
        return unserialize(base64_decode($this->getServerOriginal()));
    }

    public function getServerOriginal() {
        return $this->request->input("server");
    }

    public function __toString()
    {
        return json_encode($this->request->all());
    }
}