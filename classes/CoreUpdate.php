<?php namespace Jd\Autumn\Classes;

use Log;
use Config;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use System\Classes\UpdateManager as OctoberUpdateManager;
use Jd\Autumn\Classes\CoreUpdateRequest;
use Jd\Autumn\Models\Plugin as PluginModel;

/**
 * Return to client
 * - Plugin updates array
 * - Core updates
 * - Theme updates
 */
class CoreUpdate
{
    const OCTOBER_CMS_URL = 'https://octobercms.com/api';

    private $request = null;
    private $response = null;

    public function __construct(Request $request) {
        $this->request = new CoreUpdateRequest($request);
        $this->octoberUpdateManager = OctoberUpdateManager::instance();
    }

    public function process() {
        $pluginsToUpdate = $this->request->getPlugins();

        Log::info(__METHOD__, ["plugins" => $pluginsToUpdate]);

        $response = ["update" => 0];
        $proxyToOctober = [];

        foreach ($pluginsToUpdate as $pluginName => $version) {
            // If the private plugin server has the plugin
            try {
                $plugin = PluginModel::findByNameOrFail($pluginName);
                Log::info(__METHOD__, ['plugin' => $plugin]);

                // TODO Check if there is an update
                if(true)
                {
                    if($response["update"] == 0)
                    {
                        $response["plugins"] = [];
                    }
                    ++$response["update"];

                    $response["plugins"][$pluginName] = [
                        // MD5 hash of Zipped plugin
                        "hash" => "b10930197591e5f1f62c11008b191bef",
                        // Latest version number of plugin
                        "version" => "1.0.3",
                        // Array of updates between current version and latest
                        "updates" => [
                            "1.0.2" => str_random(40),
                            "1.0.3" => "Hello world from private update server"
                        ]
                    ];
                }
            }
            // If not, proxy to OctoberCMS
            catch(ModelNotFoundException $e) {
                $proxyToOctober[$pluginName] = $version;
            }
        }

        Log::info(__METHOD__, ['proxyToOctober' => $proxyToOctober]);

        // TODO Check whether we should proxy the remaining plugins to OctoberCMS
        if(true)
        {
            // Now proxy remaining plugins to OctoberCMS
            $result = $this->checkOctoberCMSForUpdates($proxyToOctober);

            Log::info(__METHOD__, ['plugins to update from october' => $result]);

            $updateCount = (int) array_get($result, 'update', 0);
            if($updateCount > 0)
            {
                $pluginsFromOctober = array_get($result, 'plugins', []);
                $response["plugins"] = array_merge($response["plugins"], $pluginsFromOctober);

                // Core or theme updates could from OctoberCMS
                // Make sure to include these...
                if ($core = array_get($result, 'core')) {
                    $response["core"] = $core;
                }
                if ($themes = array_get($result, 'themes')) {
                    $response["themes"] = $themes;
                }
            }

            $response["update"]+=$updateCount;
        }

        $this->response = $response;

        // Example response
        // $this->response = [
        //     "plugins" => [
        //         "RainLab.User" => [
        //             "hash" => "b10930197591e5f1f62c11008b191bef",
        //             "version" => "1.3.3",
        //             "updates" => [
        //                 "1.3.2" => "Another interesting message...",
        //                 "1.3.3" => "Allow prevention of concurrent user sessions via the user settings."
        //             ]
        //         ]
        //     ],
        //     "update" => 1
        // ];
        return $this;
    }

    public function response() {
        return response($this->response);
    }

    private function checkOctoberCMSForUpdates($plugins) {
        $params = [
            'core' => $this->request->getCore(),
            'plugins' => serialize($plugins),
            'build' => $this->request->getBuild(),
            'force' => $this->request->getForce()
        ];

        // A little hacky...
        $originalUpdateServer = Config::get('cms.updateServer');
        Config::set('cms.updateServer', self::OCTOBER_CMS_URL);
        
        $result = $this->octoberUpdateManager->requestServerData('core/update', $params);
        
        Config::set('cms.updateServer', $originalUpdateServer);

        return $result;
    }
}