<?php namespace Jd\Autumn\Classes;

use Log;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Jd\Autumn\Classes\PluginGetRequest;
use JD\Autumn\Classes\ProxyToOctoberCMS;
use Jd\Autumn\Models\Plugin as PluginModel;

/**
 * Return to client a Zip of the request plugin
 */
class PluginGet
{
    private $request = null;

    private $response = null;
    private $proxyToOctober = false;
    private $pathToFile = null;

    public function __construct(Request $request) {
        $this->request = new PluginGetRequest($request);
    }

    public function process() {
        $pluginName = $this->request->getName();

        // If the private plugin server has the plugin
        try {
            $plugin = PluginModel::findByNameOrFail($pluginName);
            Log::info(__METHOD__, ['plugin' => $plugin]);

            // TODO
            // Download the Plugin source
            // Build Zip
            // Send to client

            $this->pathToFile = storage_path("app/plugins/RainLab.User.zip");
        }
        // If not, proxy to OctoberCMS
        catch(ModelNotFoundException $e) {
            $this->proxyToOctober = true;
        }
        
        return $this;
    }

    public function response() {
        if($this->proxyToOctober)
        {
            return (new ProxyToOctoberCMS())->proxy()->response();
        }
        else
        {
            return response()->download($this->pathToFile);
        }
    }
}