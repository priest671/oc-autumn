<?php namespace JD\Autumn\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProjectsTable extends Migration
{
    public function up()
    {
        Schema::create('jd_autumn_projects', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('october_project_id')->nullabe();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jd_autumn_projects');
    }
}
