<?php

use Illuminate\Http\Request;
use JD\Autumn\Classes\ProxyToOctoberCMS;
use Jd\Autumn\Classes\CoreUpdate;
use Jd\Autumn\Classes\PluginGet;

/**
 * Returns an update list used for checking for new updates.
 */
Route::post('api/core/update', function(Request $request) {
	Log::info($request->path(), ['input' => json_encode(Input::all())]);

	return (new CoreUpdate($request))->process()->response();
});

/**
 * Returns a plugin from the update server.
 */
Route::post('api/plugin/detail', function(Request $request) {
	Log::info($request->path(), ['input' => json_encode(Input::all())]);

	return (new ProxyToOctoberCMS())->proxy()->response();
});

Route::post('api/plugin/details', function(Request $request) {
	Log::info($request->path(), ['input' => json_encode(Input::all())]);

	return (new ProxyToOctoberCMS())->proxy()->response();
});

/**
 * Return list of popular plugins from the update server.
 */
Route::post('api/plugin/popular', function(Request $request) {
	Log::info($request->path(), ['input' => json_encode(Input::all())]);
	
	return (new ProxyToOctoberCMS())->proxy()->response();
});

Route::post('api/plugin/search', function(Request $request) {
	Log::info($request->path(), ['input' => json_encode(Input::all())]);
	
	return (new ProxyToOctoberCMS())->proxy()->response();
});

/**
 * Returns content for a plugin from the update server.
 */
Route::post('api/plugin/content', function(Request $request) {
	Log::info($request->path(), ['input' => json_encode(Input::all())]);

	return (new ProxyToOctoberCMS())->proxy()->response();
});

/**
 * Returns plugin (as download) from the update server.
 */
Route::post('api/plugin/get', function(Request $request) {
	Log::info($request->path(), ['input' => json_encode(Input::all())]);

	return (new PluginGet($request))->process()->response();
});

/**
 * Return the core (as download) from the update server.
 */
Route::post('api/core/get', function(Request $request) {
	Log::info($request->path(), ['input' => json_encode(Input::all())]);
	
	return (new ProxyToOctoberCMS())->proxy()->response();
});

/**
 * Return a theme from the update server.
 */
Route::post('api/theme/detail', function(Request $request) {
	Log::info($request->path(), ['input' => json_encode(Input::all())]);
	
	return (new ProxyToOctoberCMS())->proxy()->response();
});

/**
 * Return list of popular themes from the update server.
 */
Route::post('api/theme/popular', function(Request $request) {
	Log::info($request->path(), ['input' => json_encode(Input::all())]);
	
	return (new ProxyToOctoberCMS())->proxy()->response();
});

/**
 * Returns theme (as download) from the update server.
 */
Route::post('api/theme/get', function(Request $request) {
	Log::info($request->path(), ['input' => json_encode(Input::all())]);
	
	return (new ProxyToOctoberCMS())->proxy()->response();
});