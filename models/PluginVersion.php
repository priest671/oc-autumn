<?php 

namespace JD\Autumn\Models;

use Model;

/**
 * PluginVersion Model
 */
class PluginVersion extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jd_autumn_plugin_versions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];

    public $belongsTo = [
        'plugin' => [\JD\Autumn\Models\Plugin::class],
    ];

    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];

    public $attachOne = [
        'download' => [\System\Models\File::class],
    ];

    public $attachMany = [];

}